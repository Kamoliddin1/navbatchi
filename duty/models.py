from django.core.validators import MinValueValidator
from django.db import models
from django.utils import timezone
class Profile(models.Model):
    name = models.CharField(max_length=200)
    duty = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f"Navbatchi: {self.name}"