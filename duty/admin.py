from django.contrib import admin
from .models import Profile


class AdminProfile(admin.ModelAdmin):
    pass

admin.site.register(Profile, AdminProfile)

