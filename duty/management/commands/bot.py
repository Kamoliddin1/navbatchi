import datetime
from datetime import date

import arrow
from django.conf import settings
from django.core.management.base import BaseCommand
from django_q.models import Schedule
from telegram import Bot, Update
from telegram.ext import (
    Updater,
    CommandHandler,
    CallbackContext,
)
from telegram.utils.request import Request
from django.utils import timezone
from duty.models import Profile

GIVEN_ID = -1001192018710


def log_errors(f):
    def inner(*args, **kwargs):
        try:
            f(*args, **kwargs)
        except BaseException as e:
            error_msg = f"Error {e}"
            print(error_msg)

    return inner


# @log_errors
# def start(update, context):
#     context.bot.send_message(chat_id=update.effective_chat.id, text="I'm a bot, please talk to me!")


@log_errors
def remind_duty():
    # Telegram bot SETTINGS
    token = settings.TOKEN
    request = Request(
        connect_timeout=0.5,
        read_timeout=1.0
    )
    bot = Bot(
        request=request,
        token=token
    )

    weekday = date.today().weekday()
    if weekday < 6:
        p = Profile.objects.order_by('duty').first()
        prev_date_of_profile = p.duty.strftime('%m/%d/%y')
        p.duty = p.duty.strptime(prev_date_of_profile, '%m/%d/%y') + datetime.timedelta(days=7)
        p.save()
        print('here')
        message = f"Bugun, {p}"
        bot.send_message(chat_id=GIVEN_ID, text=message)


class Command(BaseCommand):
    help = 'Telegram_bot'

    def handle(self, *args, **options):
        try:
            now2 = timezone.now()
            date2 = datetime.datetime(now2.year, now2.month, now2.day)
            next_d = date2.replace(hour=12, minute=0, second=0)
            Schedule.objects.update_or_create(
                func='duty.management.commands.bot.remind_duty',
                defaults={
                    'schedule_type': Schedule.DAILY,
                    'repeats': -1,
                    'next_run': next_d
                }
            )

        except Exception as e:
            print(e)

        # token = settings.TOKEN
        # request = Request(
        #     connect_timeout=0.5,
        #     read_timeout=1.0
        # )
        # bot = Bot(
        #     request=request,
        #     token=token
        # )

        # updater = Updater(bot=bot, use_context=True)
        # dispatcher = updater.dispatcher
        # # duty_handler = MessageHandler(Filters.text, remind_duty)
        # start_handler = CommandHandler('start', start)

        # dispatcher.add_handler(start_handler)
        # dispatcher.add_handler(duty_handler)
        # Schedule.objects
        # Schedule.objects.create(
        #     func='duty.management.commands.bot.remind_duty',
        #     schedule_type= Schedule.DAILY,
        #     # minutes= 1,
        #     repeats= 1,
        #     next_run=timezone.now().replace(hour=0, minute=15)
        # )

        # updater.start_polling()
        # updater.idle()
